import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import loginApi from '../../api/login';
import { saveToken } from '../../cache/cacheUtil';
import Input from '../../components/input';
import Button from "../../components/button";
import { PASSWORD, USERNAME } from "../../const/login";

export default function Login() {

  const [username, setUsername] = useState('');
  const [password, setPassord] = useState('');

  const history = useHistory();


  function _onChangeInput(value, type) {
    if (type === USERNAME) {
      setUsername(value);
    } else {
      setPassord(value);
    }
  }

  function _login(e) {
    e.preventDefault();
    const user = { username, password };
    loginApi.login(user)
      .then(result => {
        saveToken(result.token);
        history.push('/users');
      })
      .catch(err => console.log(err));
  }

  return (
    <div className="container-login">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6 col-lg-4 mx-auto my-3">
            <form className="bg-white p-4" onSubmit={_login}>
              <img src="http://www.templatewatch.com/royalui/template/images/logo.svg" alt="logo" className="mb-4" />
              <p className="mb-0">Hello! let's get started</p>
              <p className="mb-4">Sign in to continue</p>
              <Input onChange={(e) => _onChangeInput(e.target.value, USERNAME)} value={username}/>
              <Input onChange={(e) => _onChangeInput(e.target.value, PASSWORD)} value={password} type="password"/>
              <Button className="btn-primary">SIGN IN</Button>
              <div className="d-flex justify-content-between my-3">
                <div className="d-flex align-items-center">
                  <input type="checkbox" className="mr-2 " />
                  <small>Keep me signed in</small>
                </div>
                <small>Forgot password?</small>
              </div>
              <Button className="btn-facebook">Connect using facebook</Button>
              <div className="mt-3 text-center">
                <span>Don't have an account?</span>
                <span className="text-primary btn p-0" onClick={() => history.push('/register')}>Create</span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}