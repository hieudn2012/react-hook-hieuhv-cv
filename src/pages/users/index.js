import React, { useEffect, useState } from 'react';
import userApi from '../../api/users';
import { useHistory } from 'react-router-dom';

function Users() {

  const [data, setData] = useState([]);

  const history = useHistory();

  useEffect(() => {
    userApi.getUsers()
      .then(result => {
        setData(result || []);
      })
      .catch(err => history.push('/'));
  }, [history]);

  return (
    <div className="container py-5">
      <div className="row">
        <div className="col-md-10 mx-auto">
          <div className="table-responsive">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Avatar</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone</th>
                </tr>
              </thead>
              <tbody>
                {data.map((user, index) =>
                  <tr key={index.toString()}>
                    <td>
                      <img src={user.avatar} alt="avatar" className="rounded-circle" width={40} />
                    </td>
                    <td>{user.name}</td>
                    <td>{user.email}</td>
                    <td>{user.phone}</td>
                  </tr>)}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Users; 