import React from 'react';
import { useHistory } from 'react-router-dom';

export default function Register() {

  const history = useHistory();

  return (
    <div className="container-login">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6 col-lg-4 mx-auto my-3">
            <div className="bg-white p-4">
              <img src="http://www.templatewatch.com/royalui/template/images/logo.svg" alt="logo" className="mb-4" />
              <p className="mb-0">New here?</p>
              <p className="mb-4">Signing up is easy. It only takes a few steps</p>
              <div className="form-group">
                <input className="form-control form-control-lg rounded-0" placeholder="Username" />
              </div>
              <div className="form-group">
                <input className="form-control form-control-lg rounded-0" placeholder="Email" />
              </div>
              <div className="form-group">
                <select className="form-control form-control-lg rounded-0">
                  <option>Viet Nam</option>
                  <option>United Kingdom</option>
                  <option>Gemany</option>
                  <option>India</option>
                </select>
              </div>
              <div className="form-group">
                <input className="form-control form-control-lg rounded-0" placeholder="Password" />
              </div>
              <div className="d-flex align-items-center">
                <input type="checkbox" className="mr-2 " />
                <small>I agree to all Terms & Conditions</small>
              </div>
              <button className="btn btn-primary btn-block py-2 rounded-0 mt-3">SIGN UP</button>
              <div className="mt-3 text-center">
                <span>Already have an account?</span><span className="text-primary btn p-0" onClick={() => history.push('/')}>Login</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}