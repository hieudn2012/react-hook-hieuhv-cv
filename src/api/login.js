import axiosClient from "./axiosClient";

const loginApi = {
  login: (body) => {
    const url = '/login';
    return axiosClient.post(url, body);
  },
}

export default loginApi; 