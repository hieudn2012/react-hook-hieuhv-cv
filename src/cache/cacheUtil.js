const TOKEN = 'TOKEN';

function saveToken(token) {
  localStorage.setItem(TOKEN, token);
}

function getToken() {
  return localStorage.getItem(TOKEN);
}

function clearToken() {
  localStorage.removeItem(TOKEN);
}

export {
  saveToken,
  getToken,
  clearToken,
}