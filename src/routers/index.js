import React from 'react';
import {
  Switch,
  Route,
} from "react-router-dom";
import Users from '../pages/users';
import Login from '../pages/login';
import Register from '../pages/register';

export default function RouterConFig() {
  return (
    <Switch>
      <Route exact path="/">
        <Login />
      </Route>
      <Route path="/users">
        <Users />
      </Route>
      <Route path="/register">
        <Register />
      </Route>
    </Switch>
  )
}