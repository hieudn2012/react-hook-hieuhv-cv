import { ACTION } from "../actions/users";

const initialState = {
  users: [],
}
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION.SET_USERS_DATA: {
      return {
        users: action.payload,
      }
    }

    default:
      return state;
  }
};
export default userReducer;