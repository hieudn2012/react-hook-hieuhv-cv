export const ACTION = {
  SET_USERS_DATA: 'SET_USERS_DATA',
}

export const setUsers = (users) => {
  return {
    type: ACTION.SET_USERS_DATA,
    payload: users,
  }
}