import React from 'react';

export default function Input(props) {
  const { className } = props;
  return (
    <div className="form-group">
      <input
        { ...props }
        className={ `form-control form-control-lg rounded-0 ${className}` }
      />
    </div>
  );
}