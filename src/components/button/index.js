import React from "react";

export default function Button(props) {
  const { children, className } = props;
  return (
    <button
      { ...props }
      className={ `btn btn-block rounded-0 py-2 ${ className }` }
    >
      { children }
    </button>
  );
}